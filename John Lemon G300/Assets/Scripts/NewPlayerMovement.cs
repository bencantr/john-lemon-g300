﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using UnityEngine;
using TMPro;

public class NewPlayerMovement : MonoBehaviour
{
    //declare variables and stuff
    public float turnSpeed = 40f;
    public TextMeshProUGUI countText;
    private int count;
    private int previouscount;
    public float timeRemaining = 1;
    public bool timerIsRunning = false;
    public bool CanSprint = false;
    public TextMeshProUGUI sprintText;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    float isHorizontal = 0;
    float isVertical = 2;
    bool facingFront = true;
    //float turnAmmount = 0;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        SetCountText();
    }
    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();
        
    }
    void FixedUpdate()
    {
        float horizontal = isHorizontal;
        float vertical = isVertical;
        
        if (facingFront == true)
        {
            isHorizontal = Input.GetAxis("Horizontal");
        }
        else
        {
            isVertical = Input.GetAxis("Vertical");
        }


        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();
        if (CanSprint == true)
        {
            sprintText.text = "Boost Avaliable!";
        }
        else
        {
            sprintText.text = " ";
        }


        bool isWalking = true;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "TurnR")
        {
            UnityEngine.Debug.Log("turn");
            //turnAmmount += 90;
            //gameObject.transform.rotation = (0f, turnAmmount, 0f);
            //Vector3 to = new Vector3(0, turnAmmount, 0);
            isHorizontal = 2;
            isVertical = 0;
            facingFront = false;
        }

        else if (other.gameObject.tag == "TurnL")
        {
            UnityEngine.Debug.Log("turn");
            isHorizontal = -2;
            isVertical = 0;
            facingFront = false;
        }

        else if (other.gameObject.tag == "TurnF")
        {
            UnityEngine.Debug.Log("turn");
            isHorizontal = 0;
            isVertical = 2;
            facingFront = true;
        }
        else if (other.gameObject.CompareTag("Coin"))
        {
            other.gameObject.SetActive(false);
            count += 25;

            SetCountText();

        }
    }
    void OnAnimatorMove()
    {
        if (timerIsRunning)
        {
            m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude * 2.8f);
            m_Rigidbody.MoveRotation(m_Rotation);
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                UnityEngine.Debug.Log("Time has run out!");
                //timeRemaining = 0;
                timerIsRunning = false;
                timeRemaining = 1;

            }
        }
        else
        {
            m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude * 2);
            m_Rigidbody.MoveRotation(m_Rotation);
        }
        //m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude *2);
        //m_Rigidbody.MoveRotation(m_Rotation);

    }
    void Update()
    {

        if (count == previouscount + 250)
        {
            UnityEngine.Debug.Log("you get to sprint now");
            previouscount += 250;
            CanSprint = true;
        }
        if (Input.GetKeyDown(KeyCode.LeftShift) && CanSprint)
        {
            UnityEngine.Debug.Log("Left Shift key was pressed");
            timerIsRunning = true;
            CanSprint = false;
        }
        
    }

}